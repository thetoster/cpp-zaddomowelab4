#include <iostream>
#include "funkcje.cpp"

int main() {
    TSamochod DB[100];

    std::cout << "---" << std::endl;
    std::cout << "Initializing data" << std::endl;
    initializeData(DB);
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Dodajemy auta" << std::endl;
    TSamochod tmp1, tmp2, tmp3, tmp4;
    tmp1.carName = "Toyota";
    tmp1.carModel = "Yaris";
    tmp1.productionYear = 2015;
    tmp1.ifExists = true;
    tmp2.carName = "Honda";
    tmp2.carModel = "Civic";
    tmp2.productionYear = 2010;
    tmp2.ifExists = true;
    tmp3.carName = "BMW";
    tmp3.carModel = "E36 Coupe";
    tmp3.productionYear = 1998;
    tmp3.ifExists = true;
    tmp4.carName = "Renault";
    tmp4.carModel = "Megane Scenic";
    tmp4.productionYear = 2000;
    tmp4.ifExists = true;
    addCarToDB(DB, tmp1);
    addCarToDB(DB, tmp2);
    addCarToDB(DB, tmp3);
    addCarToDB(DB, tmp4);
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Pokaz wszystkie auta korzystajac z pointerow" << std::endl << std::endl;
    showDBUsingPointers(DB);
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "\"Renault\" ma index: " << searchDB(DB, "Renault") << std::endl;
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Usuwamy \"Renault\"" << std::endl;
    deleteItemFromDB(DB, searchDB(DB, "Renault"));
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Modyfikujemy Yariske" << std::endl;
    TSamochod tmp5;
    tmp5.carName = "Chevrolet";
    tmp5.carModel = "Camaro";
    tmp5.productionYear = 2018;
    tmp5.ifExists = true;
    modifyElementInDB(DB, searchDB(DB, "Toyota"), tmp5);
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Pokaz wszystkie auta" << std::endl << std::endl;
    showDB(DB);
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Usuwamy baze" << std::endl;
    deleteDB(DB);
    std::cout << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Pokaz wszystkie auta" << std::endl << std::endl;
    showDB(DB);
    std::cout << std::endl;

    return 0;
}

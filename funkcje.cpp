//
// Created by TheToster on 2020-11-21.
//
struct TSamochod {
    std::string carName;
    std::string carModel;
    int productionYear;
    bool ifExists;
};

void initializeData(TSamochod array[]) {
    for (int i = 0; i < 100; i++) {
        array[i].carName = "";
        array[i].carModel = "";
        array[i].productionYear = 0;
        array[i].ifExists = false;
    }
}

bool addCarToDB(TSamochod array[], TSamochod newCar) {
    for (int i = 0; i < 100; i++) {
        if (!array[i].ifExists) {
            array[i].carName = newCar.carName;
            array[i].carModel = newCar.carModel;
            array[i].productionYear = newCar.productionYear;
            array[i].ifExists = newCar.ifExists;
            return true;
        }
    }
    return false;
}

void showDB(TSamochod array[]) {
    for (int i = 0; i < 100; i++) {
        if (array[i].ifExists) {
            std::cout << "Nazwa samochodu: " << array[i].carName << std::endl;
            std::cout << "Marka samochodu: " << array[i].carModel << std::endl;
            std::cout << "Rok produkcji samochodu: " << array[i].productionYear << std::endl << std::endl;
        }
    }
}

void showDBUsingPointers(TSamochod *array) {
    for (int i = 0; i < 100; i++) {
        if ((*(array + i)).ifExists) {
            std::cout << "Nazwa samochodu: " << (*(array + i)).carName << std::endl;
            std::cout << "Marka samochodu: " << (*(array + i)).carModel << std::endl;
            std::cout << "Rok produkcji samochodu: " << (*(array + i)).productionYear << std::endl << std::endl;
        }
    }
}

void deleteDB(TSamochod array[]) {
    for (int i = 0; i < 100; i++) {
        array[i].carName = "";
        array[i].carModel = "";
        array[i].productionYear = 0;
        array[i].ifExists = false;
    }
}

int searchDB(TSamochod array[], std::string searchingPhrase) {
    for (int i = 0; i < 100; i++) {
        if (array[i].carName == searchingPhrase) return i;
    }
    return -1;
}

void deleteItemFromDB(TSamochod array[], int elementIDToDelete) {
    for (int i = 0; i < 100; i++) {
        if (i == elementIDToDelete) {
            array[i].carName = "";
            array[i].productionYear = 0;
            array[i].carModel = "";
            array[i].ifExists = false;
            break;
        }
    }
}

void modifyElementInDB(TSamochod array[], int elementIdToChange, TSamochod newCarData) {
    for (int i = 0; i < 100; i++) {
        if (i == elementIdToChange) {
            array[i].carName = newCarData.carName;
            array[i].productionYear = newCarData.productionYear;
            array[i].carModel = newCarData.carModel;
            array[i].ifExists = newCarData.ifExists;
            break;
        }
    }
}
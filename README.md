# Zadanie 4 - cpp

## Założenia
1. Dane przechowywanie w strykturze TSamochod
    - string carName
    - int productionYear
    - string carModel
    - bool ifExists
2. Struktury opisujące poszczególne samochody przechowywane są w tablicy
3. Maksymalna liuczba elementów w bazie danych 100 (max rozmiar tablicy - 100)

## Funkcjonalność

### Inicjalizacja
- argumenty: tablica 100-elementowa typu TSamochod
- funkcja ustawia ifExists na false

### Dodawanie samochodów do bazy danych
- funkcja szuka pierwszego napotkanego elementu w tablicy, która ma ifExists == false
- ustawiamy dla tej komórki odpowiednie wartości
- zwraca true/false zależnie do tego, czy było miejsce na dodanie nowego auta

### Wypisywanie samochodów z bazy wykorzystując wskaźniki
- przekazujemy tablicę jako `TSamochod * tab`
- przechodzimy po elementach przy pomocy `(*(tab+i))`

### Usuwanie całej zawartości bazy danych
- przechodzimy po elementach i ustawiamy ifExists na false

### Zapis i odczyt bazy z/do pliku
- każdy element tablicy jako wiersz w pliku txt

### Wyszukiwanie elementu po nazwie
- zwracamy index z naszej tablicy

### Usuwanie elementu
- przyjmuje index elementu do usunięcia

### Modyfikacja elementu
- przyjmujemy index i po kolei wszystkie dane modyfikujemy
- jak user nic nie wpisze, to nie edytujemy tej rzeczy